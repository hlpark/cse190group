package com.example.kenny.pawpal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.facebook.model.GraphUser;


public class StartActivity extends Activity {

    private GraphUser user;
    private ImageButton profileButton, mapButton, calendarButton, feedButton;
    private ProfileActivity pa = new ProfileActivity();
    private CalendarActivity ca = new CalendarActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        setUpUIElements();

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()) {
                case R.id.profileButton:
                    pa.setUser(user);

                    Intent intent = new Intent(getApplicationContext(), pa.getClass()); //fixed
                    startActivity(intent);
                    break;
                case R.id.mapButton:
                    Intent intent2 = new Intent(getApplicationContext(), MapsActivity.class); //fixed
                    startActivity(intent2);
                    break;
                case R.id.calendarButton:
                    //ca.setUser(user);
                    //Log.d("user??????????",user.getId());
                    Intent intent3 = new Intent(getApplicationContext(), ca.getClass());
                    startActivity(intent3);
                    break;
                case R.id.feedButton:
                    Intent intent4 = new Intent(getApplicationContext(), FeedActivity.class);
                    startActivity(intent4);
                    break;
            }
        }
    };


    private void setUpUIElements() {
        profileButton = (ImageButton) findViewById(R.id.profileButton);
        mapButton = (ImageButton) findViewById(R.id.mapButton);
        calendarButton = (ImageButton) findViewById(R.id.calendarButton);
        feedButton = (ImageButton) findViewById(R.id.feedButton);
        profileButton.setOnClickListener(onClickListener);
        mapButton.setOnClickListener(onClickListener);
        calendarButton.setOnClickListener(onClickListener);
        feedButton.setOnClickListener(onClickListener);
    }

    public void setUser(GraphUser user) {
        this.user = user;
    }

}
