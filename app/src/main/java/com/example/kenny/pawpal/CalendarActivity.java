package com.example.kenny.pawpal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.model.GraphUser;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalendarActivity extends Activity {

    public Calendar month;
    public CalendarAdapter adapter;
    public Handler handler;
    public ArrayList<String> items; // container to store some random calendar items
    public String petBirth = "0000/00/00";
    private String newDate = "0000/00/00";
    private String compare = "0000/00/00";
    public Map<String, String> vacs = new HashMap<String, String>();
    private Button preButton, upButton;
    private ArrayList<String> previous = new ArrayList<String>();
    private ArrayList<String> upcoming = new ArrayList<String>();
    private ArrayList<String> updesc = new ArrayList<String>();
    private ArrayList<String> merged = new ArrayList<String>();
    //private Pet pet;
    //private AddPetActivity apa = new AddPetActivity();
    //private ProfileActivity pa = new ProfileActivity();
    //private ParseUser userid;
    private String pet;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        //user.getUsername();
        //pet = apa.getCurrentPetPhoto();
        //Log.d("pet?????????",user.getId());

        //setUser();
        vacs.clear();
        previous.clear();
        upcoming.clear();
        updesc.clear();
        getPrev();
        //calculateCat();
        //update2();
        month = Calendar.getInstance();
        onNewIntent2();
        setUser();
        //setBirthday();
        //petBirth="";

        //if(pet.getPrevious()!=null) previous = pet.getPrevious();
        //need to fix to retrieve and save values from parse.com
        preButton = (Button) findViewById(R.id.prevButton);
        upButton = (Button) findViewById(R.id.upcomButton);
        //reButton = (Button) findViewById(R.id.recalButton);
        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                AlertDialog.Builder builder=new AlertDialog.Builder(CalendarActivity.this);
                builder.setTitle("Previous Vaccinations");

                //final CharSequence str[]={"Red","Yellow","Green","Orange","Blue"};
                final CharSequence str[] = previous.toArray(new CharSequence[previous.size()]);

                builder.setItems(str, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(MapsActivity.this, str[position] + " is located at " + adr[position], Toast.LENGTH_LONG).show();
                    }
                });

                AlertDialog dialog=builder.create();
                dialog.show();
            }
        });

        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                merged.clear();
                for (int i = 0; i < upcoming.size(); i++) {
                    merged.add(upcoming.get(i)+" - "+updesc.get(i));
                    //Log.d("here???",";hererE?????");
                }
                Collections.sort(merged);
                for(int i =0;i<previous.size();i++) {
                    merged.remove(previous.get(i));
                }


                final CharSequence[] items = merged.toArray(new CharSequence[merged.size()]);
                // arraylist to keep the selected items
                final ArrayList seletedItems=new ArrayList();


                AlertDialog.Builder builder = new AlertDialog.Builder(CalendarActivity.this);
                builder.setTitle("Select if have been already vaccinated");
                builder.setMultiChoiceItems(items, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int indexSelected,
                                                boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    seletedItems.add(indexSelected);
                                } else if (seletedItems.contains(indexSelected)) {
                                    // Else, if the item is already in the array, remove it
                                    seletedItems.remove(Integer.valueOf(indexSelected));
                                }
                            }
                        })
                        // Set the action buttons
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //  Your code when user clicked on OK
                                //  You can write the code  to save the selected item here
                                CharSequence[] copy = items.clone();
                                for(int i = 0; i<seletedItems.size(); i++) {
                                    previous.add((String) copy[(Integer) seletedItems.get(i)]);
                                }
                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
                                query.getInBackground(pet, new GetCallback<ParseObject>() {
                                    public void done(ParseObject Info, ParseException e) {
                                        if (e == null) {
                                            for (int i =0;i<previous.size(); i++) {
                                                Info.addUnique("previousList", previous.get(i));
                                                Info.saveInBackground();
                                            }
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                            }
                        });

                AlertDialog dialog = builder.create();//AlertDialog dialog; create like this outside onClick
                dialog.show();
            }
        });
/*
        reButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(CalendarActivity.this);
                alert.setTitle("Enter new start date");

// Set an EditText view to get user input
                final EditText et = new EditText(CalendarActivity.this);
                et.setInputType(InputType.TYPE_CLASS_NUMBER);

                final TextWatcher input = new TextWatcher() {
                    private String current = "";
                    private String yyyymmdd = "YYYYMMDD";
                    private Calendar cal = Calendar.getInstance();

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (!s.toString().equals(current)) {
                            String clean = s.toString().replaceAll("[^\\d.]", "");
                            String cleanC = current.replaceAll("[^\\d.]", "");

                            int cl = clean.length();
                            int sel = cl;
                            for (int i = 2; i <= cl && i < 6; i += 2) {
                                sel++;
                            }
                            //Fix for pressing delete next to a forward slash
                            if (clean.equals(cleanC)) sel--;

                            if (clean.length() < 8){
                                clean = clean + yyyymmdd.substring(clean.length());
                            }else{
                                //This part makes sure that when we finish entering numbers
                                //the date is correct, fixing it otherwise
                                int year  = Integer.parseInt(clean.substring(0,4));
                                int mon  = Integer.parseInt(clean.substring(4,6));
                                int day = Integer.parseInt(clean.substring(6,8));

                                if(mon > 12) mon = 12;
                                cal.set(Calendar.MONTH, mon-1);
                                day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                                year = (year<1900)?1900:(year>2100)?2100:year;
                                clean = String.format("%02d%02d%02d",year, mon, day);
                            }

                            clean = String.format("%s/%s/%s", clean.substring(0, 4),
                                    clean.substring(4, 6),
                                    clean.substring(6, 8));

                            sel = sel < 0 ? 0 : sel;
                            current = clean;
                            et.setText(current);
                            et.setSelection(sel   < current.length() ? sel : current.length());
                            newDate = current;

                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }



                    @Override
                    public void afterTextChanged(Editable s) {

                    }

                    // used for birthday input

                };
                et.addTextChangedListener(input);
                alert.setView(et);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
                        query.getInBackground(pet, new GetCallback<ParseObject>() {
                            public void done(ParseObject Info, ParseException e) {
                                if (e == null) {
                                    Info.put("petVacdate",newDate);
                                    Info.saveInBackground();
                                }
                            }
                        });
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                });

                alert.show();
            }
        }); */


        items = new ArrayList<String>();
        adapter = new CalendarAdapter(this, month);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(adapter);

        handler = new Handler();
        handler.post(calendarUpdater);

        TextView title  = (TextView) findViewById(R.id.title);
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        TextView previous  = (TextView) findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(month.get(Calendar.MONTH)== month.getActualMinimum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR)-1),month.getActualMaximum(Calendar.MONTH),1);
                } else {
                    month.set(Calendar.MONTH,month.get(Calendar.MONTH)-1);
                }
                refreshCalendar();
            }
        });

        TextView next  = (TextView) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(month.get(Calendar.MONTH)== month.getActualMaximum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR)+1),month.getActualMinimum(Calendar.MONTH),1);
                } else {
                    month.set(Calendar.MONTH,month.get(Calendar.MONTH)+1);
                }
                refreshCalendar();

            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                TextView date = (TextView)v.findViewById(R.id.date);
                if(date instanceof TextView && !date.getText().equals("")) {

                    String day = date.getText().toString();
                    if(day.length()==1) {
                        day = "0"+day;
                    }
                    String oh = android.text.format.DateFormat.format("yyyyMM", month)+""+day;
                    if(vacs.get(oh)!=null)
                        Toast.makeText(getApplicationContext(), vacs.get(oh), Toast.LENGTH_LONG).show();

                }

            }
        });
    }

    public void refreshCalendar()
    {
        TextView title  = (TextView) findViewById(R.id.title);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        handler.post(calendarUpdater); // generate some random calendar items

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
    }

    public void onNewIntent2() {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Log.d("date22222222", date);
        String[] dateArr = date.split("-"); // date format is yyyy-mm-dd
        month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1])-1, Integer.parseInt(dateArr[2]));
    }

    public void calculateCat() {
        Log.d("bbbbbbb",petBirth);
        //if(pet.getPetBirthday()!=null) petBirth = pet.getPetBirthday();
        String[] b = petBirth.split("/");
        Calendar bday = Calendar.getInstance();
        bday.set(Integer.valueOf(b[0]), Integer.valueOf(b[1])-1, Integer.valueOf(b[2]));
        //first
        Calendar first = (Calendar)bday.clone();
        first.add(Calendar.MONTH, 2);
        String one = new SimpleDateFormat("yyyyMMdd").format(first.getTime());
        vacs.put(one, "Core Vaccination");
        //second
        Calendar second = (Calendar)bday.clone();
        second.add(Calendar.MONTH,3);
        String two = new SimpleDateFormat("yyyyMMdd").format(second.getTime());
        vacs.put(two,"Core Vaccination");
        //third
        Calendar third = (Calendar)bday.clone();
        third.add(Calendar.MONTH,4);
        String three = new SimpleDateFormat("yyyyMMdd").format(third.getTime());
        vacs.put(three,"Core Vaccination, Rabies");
        //annually
        for(int i=1;i<19;i++){
            Calendar year = (Calendar) bday.clone();
            year.add(Calendar.YEAR, i);
            String wow = new SimpleDateFormat("yyyyMMdd").format(year.getTime());
            vacs.put(wow, "Core Vaccination, Rabies");
        }
        ArrayList<String> keys = new ArrayList<String>(vacs.keySet());
        for (String s : keys) {
            upcoming.add(s);
            updesc.add(vacs.get(s));
        }
        refreshCalendar();
        getPrev();
    }

    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
           // merged.clear();
            items.clear();

            // format random values. You can implement a dedicated class to provide real values
			/*for(int i=0;i<31;i++) {
				Random r = new Random();

				if(r.nextInt(10)>6)
				{
					items.add(Integer.toString(i));
				}
			}   */

            //String b = Arrays.toString(bday);
            //items.add(b);
            //Log.d("aaaaa",b);
            ArrayList<String> keys = new ArrayList<String>(vacs.keySet());
            for (String s : keys) {
                items.add(s);
            }

            adapter.setItems(items, vacs);
            adapter.notifyDataSetChanged();
        }
    };

    public void setUser() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.whereEqualTo("author", ParseUser.getCurrentUser());

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> gru, ParseException e) {
                if (e == null && gru.size()!=0) {
                    pet = gru.get(0).getObjectId();
                    //petBirth = (String) gru.get(0).get("petBirthday");
                    //onNewIntent2();
                    //calculateCat();
                    setBirthday();
                } else {
                    //finish();
                    Toast.makeText(CalendarActivity.this, "Please add your pet first.", Toast.LENGTH_LONG).show();
                    finish();
                    //Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }
/*
    public void update2(List<String> listt) {
       ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.getInBackground("ag9BHh9oji", new GetCallback<ParseObject>() {
            public void done(ParseObject gameScore, ParseException e) {
                if (e == null) {
                    // Now let's update it with some new data. In this case, only cheatMode and score
                    // will get sent to the Parse Cloud. playerName hasn't changed.
                    for (int i =0;i<listt.size(); i++) {
                        gameScore.addUnique("previousList", listt.get(i));
                        gameScore.saveInBackground();
                    }
                }
            }
        });
    }*/

    private void setBirthday() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.getInBackground(pet, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                Log.d("bbbb",Boolean.toString(object.has("petBirthday")));
                Log.d("bb",object.getString("petBirthday"));
                if (e == null) {

                    petBirth = object.getString("petVacdate");
                    Log.d("bb",petBirth);
                    calculateCat();
                } else {
                    Toast.makeText(CalendarActivity.this, "Please add your pet's birthday.", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
       // calculateCat();
    }

    private void getPrev() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.getInBackground(pet, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null && object.getList("previousList")!=null) {
                    List<String> obj = object.getList("previousList");
                    ArrayList<String> temp = new ArrayList<String>(obj);
                    previous = temp;
                    //Log.d("whattttttttttt",temp.get(0));
                } else {

                }
            }
        });
    }
}


