package com.example.kenny.pawpal;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by hyelimpark on 15. 3. 14..
 */
public class FakeUser {
    public double lat;
    public double lng;
    public String name;
    public String birthday;
    public int gender;
    public boolean petsitting;

    public FakeUser(double one, double one1, String two, String three, int four, boolean five) {
        this.lat = one;
        this.lng = one1;
        this.name = two;
        this.birthday = three;
        this.gender = four;
        this.petsitting = five;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getName() {
        return name;
    }

    public String getBirthday() {
        return birthday;
    }

    public int getGender() {
        return gender;
    }

    public boolean getPetsitting() {
        return petsitting;
    }

}
