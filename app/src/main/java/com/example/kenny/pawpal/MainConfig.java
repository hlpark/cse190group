package com.example.kenny.pawpal;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by Kenny on 2/25/15.
 */
public class MainConfig {

    private Collection<String> facebookLoginPermissions;

    public Collection<String> getFacebookLoginPermissions() {
        if (facebookLoginPermissions != null) {
            return Collections.unmodifiableCollection(facebookLoginPermissions);
        } else {
            return null;
        }
    }

}
