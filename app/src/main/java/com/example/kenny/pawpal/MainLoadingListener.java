package com.example.kenny.pawpal;

/**
 * Created by Kenny on 2/25/15.
 */
public interface MainLoadingListener {

    public void onLoadingStart(boolean showSpinner);

    public void onLoadingFinish();
}
