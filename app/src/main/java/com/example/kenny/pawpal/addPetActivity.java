package com.example.kenny.pawpal;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.view.WindowManager;


public class AddPetActivity extends ActionBarActivity {


    private Pet photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        photo = new Pet();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);


        // Begin with main data entry view,
        // NewMealFragment
        setContentView(R.layout.activity_add_pet);
        FragmentManager manager = getFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {
            fragment = new NewPetPhotoFragment();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment)
                    .commit();
        }
    }

    public Pet getCurrentPetPhoto() {
        return photo;
    }
}
