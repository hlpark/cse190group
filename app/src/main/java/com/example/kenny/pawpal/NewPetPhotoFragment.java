package com.example.kenny.pawpal;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Calendar;
import java.util.List;

/*
 * This fragment manages the data entry for a
 * new Meal object. It lets the user input a 
 * meal name, give it a rating, and take a 
 * photo. If there is already a photo associated
 * with this meal, it will be displayed in the 
 * preview at the bottom, which is a standalone
 * ParseImageView.
 */
public class NewPetPhotoFragment extends Fragment {

    static final int REFRESH = 1;

    private ImageButton photoButton;
    private Button saveButton;
    private Button cancelButton;
    private TextView photoName;
    private ParseImageView photoPreview;
    private EditText date;
    private TextWatcher tw;
    private String bdayString;
    private RadioGroup genderRadio;
    private int gender;     // 0 = male  1 = female
    private String petId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle SavedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_pet_photo, parent, false);


        genderRadio = (RadioGroup) v.findViewById(R.id.radioSex);
        genderRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.radioMale) {
                    gender = 0;
                } else if( checkedId == R.id.radioFemale) {
                    gender = 1;
                }
            }
        });


        photoName = ((EditText) v.findViewById(R.id.pet_name));

        // The mealRating spinner lets people assign favorites of meals they've
        // eaten.


        photoButton = ((ImageButton) v.findViewById(R.id.photo_button));
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(photoName.getWindowToken(), 0);
                startCamera();
            }
        });

        saveButton = ((Button) v.findViewById(R.id.save_button));
        saveButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidity() == false) {
                    return;
                }
                final Pet pet = ((AddPetActivity) getActivity()).getCurrentPetPhoto();

                // When the user clicks "Save," upload the meal to Parse
                // Add data to the meal object:
                pet.setTitle(photoName.getText().toString());

                // Associate the meal with the current user
                pet.setAuthor(ParseUser.getCurrentUser());

                // If the user added a photo, that data will be
                // added in the CameraFragment

                pet.setPetBirthday(bdayString);
                pet.setPetVacday(bdayString);

                pet.setGender(gender);

                ParseUser currentUser = ParseUser.getCurrentUser();


                //privateData.setACL(new ParseACL(currentUser));

                // Save the pet and return
                pet.saveInBackground(new SaveCallback() {

                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            //getActivity().setResult(Activity.RESULT_OK);
                            //getActivity().finish();
                            //Intent intent = new Intent(getActivity().getApplicationContext(), ProfileActivity.class); //fixed
                            //startActivity(intent);
                            setid();
                            Log.d("done?????","wywywyyw");

                        } else {
                            Toast.makeText(
                                    getActivity().getApplicationContext(),
                                    "Error saving: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                });





            }
        });

        cancelButton = ((Button) v.findViewById(R.id.cancel_button));
        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                getActivity().finish();
            }
        });

        // Until the user has taken a photo, hide the preview
        photoPreview = (ParseImageView) v.findViewById(R.id.pet_preview_image);
        photoPreview.setVisibility(View.INVISIBLE);


        tw = new TextWatcher() {
        private String current = "";
        private String yyyymmdd = "YYYYMMDD";
        private Calendar cal = Calendar.getInstance();

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8){
                        clean = clean + yyyymmdd.substring(clean.length());
                    }else{
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int year  = Integer.parseInt(clean.substring(0,4));
                        int mon  = Integer.parseInt(clean.substring(4,6));
                        int day = Integer.parseInt(clean.substring(6,8));

                        if(mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon-1);
                        day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                        year = (year<1900)?1900:(year>2100)?2100:year;
                        clean = String.format("%02d%02d%02d",year, mon, day);
                    }

                    clean = String.format("%s/%s/%s", clean.substring(0, 4),
                            clean.substring(4, 6),
                            clean.substring(6, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    date.setText(current);
                    date.setSelection(sel   < current.length() ? sel : current.length());
                    bdayString = current;

                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }



            @Override
            public void afterTextChanged(Editable s) {

            }

            // used for birthday input

    };

        date = (EditText) v.findViewById(R.id.whichdate);
        date.addTextChangedListener(tw);

        return v;
    }

    public boolean checkValidity() {
        boolean valid = true;
        if(photoName.getText().length() == 0) {
            Toast.makeText(getActivity(), "Pet needs to have a Name", Toast.LENGTH_LONG).show();
            valid = false;
        } else if ( date.getText().length() == 0) {
            Toast.makeText(getActivity(), "Pet needs to have a Birthday", Toast.LENGTH_LONG).show();
            valid = false;
        }
        return valid;
    }

    /*
     * All data entry about a Meal object is managed from the NewMealActivity.
     * When the user wants to add a photo, we'll start up a custom
     * CameraFragment that will let them take the photo and save it to the Meal
     * object owned by the NewMealActivity. Create a new CameraFragment, swap
     * the contents of the fragmentContainer (see activity_new_meal.xml), then
     * add the NewMealFragment to the back stack so we can return to it when the
     * camera is finished.
     */
    public void startCamera() {
        Fragment cameraFragment = new CameraFragment();
        FragmentTransaction transaction = getActivity().getFragmentManager()
                .beginTransaction();
        transaction.replace(R.id.fragmentContainer, cameraFragment);
        transaction.addToBackStack("NewMealFragment");
        transaction.commit();
    }

    /*
     * On resume, check and see if a meal photo has been set from the
     * CameraFragment. If it has, load the image in this fragment and make the
     * preview image visible.
     */
    @Override
    public void onResume() {
        super.onResume();
        ParseFile photoFile = ((AddPetActivity) getActivity())
                .getCurrentPetPhoto().getPhotoFile();
        if (photoFile != null) {
            photoPreview.setParseFile(photoFile);
            photoPreview.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] data, ParseException e) {
                    photoPreview.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void setid() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.whereEqualTo("author", ParseUser.getCurrentUser());

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> listOfPets, ParseException e) {
                if (e == null && listOfPets.size()>0) {
                    petId = listOfPets.get(0).getObjectId();
                    Log.i("System.out", "the pet id is = " + petId);

                    ParseUser.getCurrentUser().put("pet1_ID", petId);
                    ParseUser.getCurrentUser().saveEventually();
                    movemove();
                } else {

                }
            }
        });
    }

    private void movemove() {
        Intent intent = new Intent(getActivity(),ProfileActivity.class); //fixed
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}