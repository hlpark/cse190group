package com.example.kenny.pawpal;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends Activity implements LocationListener {

    //instance variables for Marker icon drawable resources
    private int userIcon, hospitalIcon, storeIcon, otherIcon, friendIcon, meIcon;

    //the map
    private GoogleMap theMap;

    //location manager
    private LocationManager locMan;

    //user marker
    private Marker userMarker;

    //places of interest
    private Marker[] placeMarkers;
    //max
    private final int MAX_PLACES = 20;//most returned from google
    //marker options
    private MarkerOptions[] places;

    private Button sButton, hButton, nButton;

    private ArrayList<String> names = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    private ArrayList<String> names2 = new ArrayList<String>();
    private ArrayList<String> addresses2 = new ArrayList<String>();

    private ArrayList<String> names3 = new ArrayList<String>();
    private ArrayList<String> info1 = new ArrayList<String>();
    private ArrayList<String> info2 = new ArrayList<String>();
    private ArrayList<String> info3 = new ArrayList<String>();

    private HashMap<String, Pair<Double,Double>> map = new HashMap<String, Pair<Double,Double>>();

    //other users markers
    private Marker others;
    private String name,gender,birth,petsitter;

    private double mylat = 32.88006;
    private double mylng = -117.23401;

    private FakeUser a,b,c,d,e,f,g,h,i,j,me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);



        //get drawable IDs
        //userIcon = R.drawable.yellow_point;
        friendIcon = R.drawable.m1;
        hospitalIcon = R.drawable.m2;
        storeIcon = R.drawable.m3;
        meIcon = R.drawable.m4;

        //find out if we already have it
        if(theMap==null){
            //get the map
            theMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.the_map)).getMap();
            //check in case map/ Google Play services not available
            if(theMap!=null){
                theMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    // Use default InfoWindow frame
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoContents(Marker arg0) {
                        View v;
                        // Getting the position from the marker
                        LatLng latLng = arg0.getPosition();
                        final String petname = arg0.getTitle();
                        String petinfo = arg0.getSnippet();
                        Log.d("whattttt?????",petinfo);
                        String[] infoarray = petinfo.split("-");
                        Log.d("whattttt?????",Integer.toString(infoarray.length));

                        if(infoarray[3].contains("pet")) {
                            // Getting view from the layout file info_window_layout
                            v = getLayoutInflater().inflate(R.layout.info_window_layout, null);


                            // Getting reference to the TextView to set latitude
                            TextView tvName = (TextView) v.findViewById(R.id.tv_name);

                            // Getting reference to the TextView to set longitude
                            TextView tvGender = (TextView) v.findViewById(R.id.tv_gender);
                            TextView tvBirth = (TextView) v.findViewById(R.id.tv_birth);
                            TextView tvPetSitter = (TextView) v.findViewById(R.id.tv_petsitter);
                            TextView tap = (TextView) v.findViewById(R.id.tap);



                            tvName.setText(petname);
                            tvGender.setText(infoarray[0]);
                            tvBirth.setText(infoarray[1]);
                            tvPetSitter.setText(infoarray[2]);
                            if (infoarray[2].contains("Not")) {
                                tap.setVisibility(View.INVISIBLE);
                                theMap.setOnInfoWindowClickListener(null);

                            } else {

                                tap.setVisibility(View.VISIBLE);
                                theMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                    @Override
                                    public void onInfoWindowClick(Marker marker) {
/////alert for sending msg
                                        Log.d("clicked","!!!!");
                                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                switch (which) {
                                                    case DialogInterface.BUTTON_POSITIVE:
                                                        //Yes button clicked
                                                        //Toast.makeText(MapsActivity.this, "Request Sent", Toast.LENGTH_LONG);
                                                        Intent intent = new Intent(getApplicationContext(), FeedActivity.class); //fixed
                                                        startActivity(intent);
                                                        break;

                                                    case DialogInterface.BUTTON_NEGATIVE:
                                                        //No button clicked

                                                        break;
                                                }
                                            }
                                        };

                                        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                                        String tempname = petname.substring(0,1).toUpperCase() + petname.substring(1).toLowerCase();

                                        builder.setTitle("Do you want to send a message to " + tempname + "?")
                                                .setPositiveButton("Yes", dialogClickListener)
                                                .setNegativeButton("No", dialogClickListener).show();
                                    }
                                });

                            }

                        }



                        else {
                            //stores or hospitals
                            theMap.setOnInfoWindowClickListener(null);
                            v = getLayoutInflater().inflate(R.layout.info_window_layout2, null);


                            // Getting reference to the TextView to set latitude
                            TextView tvName = (TextView) v.findViewById(R.id.tv_what);

                            // Getting reference to the TextView to set longitude
                            TextView tvGender = (TextView) v.findViewById(R.id.tv_address);



                            tvName.setText(petname);
                            tvGender.setText(infoarray[0]);
                        }

                        // Returning the view containing InfoWindow contents
                        return v;

                    }
                });





                hButton = (Button) findViewById(R.id.hospitalButton);

                hButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        AlertDialog.Builder builder=new AlertDialog.Builder(MapsActivity.this);
                        builder.setTitle("Nearby Pet Hospitals");

                        //final CharSequence str[]={"Red","Yellow","Green","Orange","Blue"};
                        final CharSequence str[] = names.toArray(new CharSequence[names.size()]);
                        final CharSequence adr[] = addresses.toArray(new CharSequence[addresses.size()]);

                        builder.setItems(str, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int position) {
                                // TODO Auto-generated method stub
                                Toast.makeText(MapsActivity.this, str[position] + " is located at " + adr[position], Toast.LENGTH_LONG).show();
                            }
                        });

                        AlertDialog dialog=builder.create();
                        dialog.show();
                    }
                });

                sButton = (Button) findViewById(R.id.storeButton);

                sButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        AlertDialog.Builder builder=new AlertDialog.Builder(MapsActivity.this);
                        builder.setTitle("Nearby Pet Stores");

                        //final CharSequence str[]={"Red","Yellow","Green","Orange","Blue"};
                        final CharSequence str[] = names2.toArray(new CharSequence[names2.size()]);
                        final CharSequence adr[] = addresses2.toArray(new CharSequence[addresses2.size()]);

                        builder.setItems(str, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int position) {
                                // TODO Auto-generated method stub
                                Toast.makeText(MapsActivity.this, str[position] + " is located at " + adr[position], Toast.LENGTH_LONG).show();
                            }
                        });

                        AlertDialog dialog=builder.create();
                        dialog.show();
                    }
                });

                nButton = (Button) findViewById(R.id.nearButton);

                nButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        AlertDialog.Builder builder=new AlertDialog.Builder(MapsActivity.this);
                        builder.setTitle("Nearby Pets");

                        //final CharSequence str[]={"Red","Yellow","Green","Orange","Blue"};
                        final CharSequence str[] = names3.toArray(new CharSequence[names3.size()]);
                        final CharSequence adr[] = info1.toArray(new CharSequence[info1.size()]);
                        final CharSequence adr2[] = info2.toArray(new CharSequence[info2.size()]);
                        final CharSequence adr3[] = info3.toArray(new CharSequence[info3.size()]);

                        builder.setItems(str, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int position) {
                                // TODO Auto-generated method stub
                                Toast.makeText(MapsActivity.this, str[position] + " is " + adr2[position] +", born on " + adr[position] + ", and currently " + adr3[position], Toast.LENGTH_LONG).show();
                            }
                        });

                        AlertDialog dialog=builder.create();
                        dialog.show();
                    }
                });
                //ok - proceed
                //theMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                //locMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
                //get last location
                //Location lastLoc = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                double lat = mylat;
                double lng = mylng;
                //double test1 = 23;
                //double test2 = 34;




                LatLng lastLatLng = new LatLng(lat, lng);
                theMap.setMyLocationEnabled(true);
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(lastLatLng, 13);
                theMap.animateCamera(yourLocation);
                //theMap.getUiSettings().setMyLocationButtonEnabled(true);
                theMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        //locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        //get last location
                        //Location lastLoc = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        double lat = mylat;
                        double lng = mylng;
                        //upload(lat,lng);
                        LatLng lastLatLng = new LatLng(lat, lng);
                        theMap.setMyLocationEnabled(true);
                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(lastLatLng, 13);
                        theMap.animateCamera(yourLocation);
                        updatePlaces();
                        return true;
                    }
                });
                //create marker array
                placeMarkers = new Marker[MAX_PLACES];
                //************* need to create markers for other users nearby(retrieve values from parse) ***********
                //check for visibility of user
                //then compare whether other users are in 1 mile range from the user
                /**** test values ******/
                //double lat2 = 32.885571;
                //double lng2 = -117.236535;
                //below should be false
                //double lat2 =32.913787;
                //double lng2 = -117.163856;
                //LatLng LatLng2 = new LatLng(lat2, lng2);
                //Log.d("1",Double.toString(lat));
                //Log.d("2",Double.toString(lng));
                //Log.d("3",Double.toString(lat2));
                //Log.d("4",Double.toString(lng2));
                //Log.d("RANGEEEEEE", Boolean.toString(inRange(lat, lng, lat2, lng2)));

                /*ArrayList<String> keys = new ArrayList<String>(map.keySet());
                ArrayList<Pair<Double,Double>> values = new ArrayList<Pair<Double,Double>>(map.values());
                Log.d("?????????????????",Integer.toString(values.size()));
                for(int i=0;i<values.size();i++) {
                    double lat2 = values.get(i).first;
                    double lng2 = values.get(i).second;
                    Log.d("RANGEEEEEE", Boolean.toString(inRange(lat, lng, lat2, lng2)));
                }*/
                /*
                if(inRange(lat, lng, lat2, lng2)) {
                    //others = new Marker[MAX_PLACES];
                    others = theMap.addMarker(new MarkerOptions()
                            .position(LatLng2)
                            .title("Other user is here")
                            .icon(BitmapDescriptorFactory.fromResource(friendIcon))
                            .snippet("TESTING"));
                }*/
                //create marker


                //update location
                updatePlaces();

                //getOthers();
            }

        }
    }

    //location listener functions

    @Override
    public void onLocationChanged(Location location) {
        Log.v("MyMapActivity", "location changed");
        //updatePlaces();
    }
    @Override
    public void onProviderDisabled(String provider){
        Log.v("MyMapActivity", "provider disabled");
    }
    @Override
    public void onProviderEnabled(String provider) {
        Log.v("MyMapActivity", "provider enabled");
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.v("MyMapActivity", "status changed");
    }

    /*
     * update the place markers
     */
    private void updatePlaces(){
        names.clear();
        addresses.clear();
        names2.clear();
        addresses2.clear();
        //get location manager
        //locMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        //get last location
        //Location lastLoc = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        double lat = mylat;
        double lng = mylng;
        upload(lat,lng);
        getOthers(lat,lng);
        //uploadAndSet(lat,lng);
        //create LatLng
        LatLng lastLatLng = new LatLng(lat, lng);

        //remove any existing marker
        if(userMarker!=null) userMarker.remove();
        //create and set marker properties
        /*
        userMarker = theMap.addMarker(new MarkerOptions()
                .position(lastLatLng)
                .title("You are here")
                .icon(BitmapDescriptorFactory.fromResource(userIcon))
                .snippet("Your last recorded location"));*/
        //move to location
        //theMap.animateCamera(CameraUpdateFactory.newLatLng(lastLatLng), 3000, null);

        //LatLng coordinate = new LatLng(lat, lng);


        //build places query string
        String types = "veterinary_care|pet_store";
        try {
            types = URLEncoder.encode(types, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                "json?location="+lat+","+lng+
                "&radius=3000" +
                "&types=" + types +
                "&key=AIzaSyBdnflPIyBm0OduNgWMfXc0M3CQB6zoJjk"; //ADD KEY

        //execute query
        //Log.d("testst",placesSearchStr);
        new GetPlaces().execute(placesSearchStr);

        //locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, this);
    }

    private class GetPlaces extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... placesURL) {
            //fetch places
            //upload(2,3);
            //build result as string
            StringBuilder placesBuilder = new StringBuilder();
            //process search parameter string(s)
            for (String placeSearchURL : placesURL) {
                HttpClient placesClient = new DefaultHttpClient();
                try {
                    //try to fetch the data

                    //HTTP Get receives URL string
                    HttpGet placesGet = new HttpGet(placeSearchURL);
                    //execute GET with Client - return response
                    HttpResponse placesResponse = placesClient.execute(placesGet);
                    //check response status
                    StatusLine placeSearchStatus = placesResponse.getStatusLine();
                    //only carry on if response is OK
                    if (placeSearchStatus.getStatusCode() == 200) {
                        //get response entity
                        HttpEntity placesEntity = placesResponse.getEntity();
                        //get input stream setup
                        InputStream placesContent = placesEntity.getContent();
                        //create reader
                        InputStreamReader placesInput = new InputStreamReader(placesContent);
                        //use buffered reader to process
                        BufferedReader placesReader = new BufferedReader(placesInput);
                        //read a line at a time, append to string builder
                        String lineIn;
                        while ((lineIn = placesReader.readLine()) != null) {
                            placesBuilder.append(lineIn);
                        }
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
            return placesBuilder.toString();
        }
        //process data retrieved from doInBackground
        protected void onPostExecute(String result) {
            //parse place data returned from Google Places
            //remove existing markers
            if(placeMarkers!=null){
                for(int pm=0; pm<placeMarkers.length; pm++){
                    if(placeMarkers[pm]!=null)
                        placeMarkers[pm].remove();
                }
            }
            try {
                //parse JSON
                //Log.d("test","1");
                //create JSONObject, pass stinrg returned from doInBackground
                JSONObject resultObject = new JSONObject(result);
                //Log.d("test2",resultObject.toString());
                //get "results" array
                JSONArray placesArray = resultObject.getJSONArray("results");
                //marker options for each place returned
                places = new MarkerOptions[placesArray.length()];
                //loop through places
                for (int p=0; p<placesArray.length(); p++) {
                    //parse each place
                    //if any values are missing we won't show the marker
                    boolean missingValue=false;
                    LatLng placeLL=null;
                    String placeName="";
                    String vicinity="";
                    int currIcon = otherIcon;
                    try{

                        //attempt to retrieve place data values
                        missingValue=false;
                        //get place at this index
                        JSONObject placeObject = placesArray.getJSONObject(p);
                        //Log.d("test",placeObject.toString());
                        //get location section
                        JSONObject loc = placeObject.getJSONObject("geometry")
                                .getJSONObject("location");
                        //read lat lng
                        placeLL = new LatLng(Double.valueOf(loc.getString("lat")),
                                Double.valueOf(loc.getString("lng")));
                        //get types
                        JSONArray types = placeObject.getJSONArray("types");
                        //loop through types
                        for(int t=0; t<types.length(); t++){
                            //what type is it
                            String thisType=types.get(t).toString();
                            //check for particular types - set icons
                            if(thisType.contains("veterinary_care")){
                                currIcon = hospitalIcon;
                                names.add(placeObject.getString("name"));
                                addresses.add(placeObject.getString("vicinity"));
                                break;
                            }
                            else if(thisType.contains("pet_store")){
                                currIcon = storeIcon;
                                names2.add(placeObject.getString("name"));
                                addresses2.add(placeObject.getString("vicinity"));
                                break;
                            }
                        }
                        //vicinity
                        vicinity = placeObject.getString("vicinity");
                        //name
                        placeName = placeObject.getString("name");
                    }
                    catch(JSONException jse){
                        Log.v("PLACES", "missing value");
                        missingValue=true;
                        jse.printStackTrace();
                    }
                    //if values missing we don't display
                    if(missingValue)	places[p]=null;
                    else
                        places[p]=new MarkerOptions()
                                .position(placeLL)
                                .title(placeName)
                                .icon(BitmapDescriptorFactory.fromResource(currIcon))
                                .snippet(vicinity + "- - -not");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if(places!=null && placeMarkers!=null){
                for(int p=0; p<places.length && p<placeMarkers.length; p++){
                    //will be null if a value was missing
                    if(places[p]!=null)
                        placeMarkers[p]=theMap.addMarker(places[p]);
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(theMap!=null){
            //locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(theMap!=null){
            //locMan.removeUpdates(this);
        }
    }

    private Boolean inRange(double lon1, double lat1, double lon2, double lat2) {
        Location locationA = new Location("point A");
        locationA.setLatitude(lat1);
        locationA.setLongitude(lon1);
        Location locationB = new Location("point B");
        locationB.setLatitude(lat2);
        locationB.setLongitude(lon2);
        final float[] results= new float[3];
        Location.distanceBetween(lat1,lon1,lat2,lon2,results);
        //float dist = locationA.distanceTo(locationB) ;
        float dist = results[0];
        Log.d("DISTTTTTTTT????",Float.toString(dist));
        //5 miles
        return dist <= 8046.72;
    }

    private void getOthers(double lat1, double lng1) {
        map.clear();
        names3.clear();
        info1.clear();
        info2.clear();
        info3.clear();
        a = new FakeUser(32.85656,-117.24079,"Cookie","2013/06/03",0,true);
        b = new FakeUser(32.85339,-117.25461,"Jack","2011/12/23",0,true);
        c = new FakeUser(32.85245,-117.22826,"Toby","2014/06/22",0,false);
        d = new FakeUser(32.86103,-117.21007,"Max","2010/05/02",0,true);
        e = new FakeUser(32.86701,-117.21067,"Buddy","2015/01/07",0,true);
        f = new FakeUser(32.88727,-117.21788,"Molly","2009/04/14",1,true);
        g = new FakeUser(32.85440,-117.20483,"Kitty","2013/07/08",1,false);
        h = new FakeUser(32.86045,-117.22577,"Daisy","2011/03/03",1,true);
        i = new FakeUser(32.89599,-117.23599,"Coco","2008/02/24",1,true);
        j = new FakeUser(32.90399,-117.21934,"Poppy","2013/03/14",1,false);
        me = new FakeUser(mylat,mylng,"Gru(Me)","2014/05/28",0,true);

        final double one = lat1;
        final double two = lng1;
        /*
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        try{Log.d("ISZEIZIEIIZEIIZE",Integer.toString(query.count()));} catch (ParseException e) {}
        //query.whereEqualTo("username", "Kenny Lee");//changetotrue!!!!
        query.whereEqualTo("allowLocation",true);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                Log.d("owijqovijwoiejvoiqjweoivjq", Integer.toString(objects.size()));
                if (e == null && objects.size() > 0) {
                    // The query was successful.
                    for (int i = 0; i < objects.size(); i++) {
                        Double first = objects.get(i).getDouble("lat");
                        Double second = objects.get(i).getDouble("lng");
                        if (inRange(one, two, first, second)) {
                            getname(objects.get(i).getString("pet1_ID"), first, second, one, two);
                        }
                        //String name = getname(objects.get(i).getString("objectId"));
                        //objects.get(i).get
                        //Log.d("nae..???????",namee);

                    }


                } else {
                    // Something went wrong.
                }
            }


        });
        */

        getname(a, a.getLat(), a.getLng(), one, two);
        getname(b, b.getLat(), b.getLng(), one, two);
        getname(c, c.getLat(), c.getLng(), one, two);
        getname(d, d.getLat(), d.getLng(), one, two);
        getname(e, e.getLat(), e.getLng(), one, two);
        getname(f, f.getLat(), f.getLng(), one, two);
        getname(g, g.getLat(), g.getLng(), one, two);
        getname(h, h.getLat(), h.getLng(), one, two);
        getname(i, i.getLat(), i.getLng(), one, two);
        getname(j, j.getLat(), j.getLng(), one, two);
        getname(me, me.getLat(), me.getLng(), one, two);


    }

    private void getname(FakeUser id, double one, double two, double lat1, double lng1) {
        final double other1 = one;
        final double other2 = two;
        final double me1 = lat1;
        final double me2 = lng1;
//        Log.d("idididididi",id);
        /*
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.whereEqualTo("objectId", id);
        //query.whereEqualTo("author",id);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> gru, ParseException e) {
                if (e == null && gru.size() != 0) {
                    name = gru.get(0).getString("title");
                    if (gru.get(0).getInt("gender") == 0) {
                        gender = "Male";
                    } else {
                        gender = "Female";
                    }
                    birth = gru.get(0).getString("petBirthday");
                    if (gru.get(0).getBoolean("allowPetSitting")==false) {
                        petsitter = "Available for pet sitting";//fix later
                    } else {
                        petsitter = "Available for pet sitting";
                    }


                    Log.d("name??????", name);
                    LatLng user = new LatLng(other1, other2);
                    //Pair<Double,Double> ll = new Pair(first,second);

                    //map.put(objects.get(i).getUsername(), ll);
                    //Log.d("1111111111111111",Integer.toString(map.size()));
                    //Log.d("RANGEEEEEE", Boolean.toString(inRange(one, two, first, second)));
                    if(me1!=other1&&me2!=other2) {
                        if (inRange(me1, me2, other1, other2)) {
                            others = theMap.addMarker(new MarkerOptions()
                                    .position(user)
                                    .title(name.toUpperCase())
                                    .icon(BitmapDescriptorFactory.fromResource(friendIcon))
                                    .snippet(birth + "-" + gender + "-" + petsitter + "-pet"));
                        }
                    } else {
                        others = theMap.addMarker(new MarkerOptions()
                                .position(user)
                                .title(name.toUpperCase())
                                .icon(BitmapDescriptorFactory.fromResource(meIcon))
                                .snippet("My location" + "- - -not"));
                    }
                } else {
                    name = "";
                    Log.d("name???", name);
                }
            }
        });
        */
        name = id.getName();
        names3.add(name);
        if (id.getGender() == 0) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        info2.add(gender.toLowerCase());
        birth = id.getBirthday();
        info1.add(birth);
        if (id.getPetsitting()==false) {
            petsitter = "Not available for pet sitting";//fix later
        } else {
            petsitter = "Available for pet sitting";
        }
        info3.add(petsitter.toLowerCase());

        LatLng user = new LatLng(other1, other2);
        if(me1!=other1&&me2!=other2) {
            if (inRange(me1, me2, other1, other2)) {
                others = theMap.addMarker(new MarkerOptions()
                        .position(user)
                        .title(name.toUpperCase())
                        .icon(BitmapDescriptorFactory.fromResource(friendIcon))
                        .snippet(birth + "-" + gender + "-" + petsitter + "-pet"));
            }
        } else {
            others = theMap.addMarker(new MarkerOptions()
                    .position(user)
                    .title(name.toUpperCase())
                    .icon(BitmapDescriptorFactory.fromResource(meIcon))
                    .snippet("My location" + "- - -not"));
        }

    }

    private void upload(double lat, double lng) {
        //*****save these lat, lng to parse to retrieve from other users********
        ParseUser puser = ParseUser.getCurrentUser();
        puser.put("lat",lat);
        puser.put("lng",lng);
        puser.put("location", true);
        puser.saveEventually();
        //puser.saveInBackground();
        //getOthers(lat,lng);
        //Log.d("latlng svaedd","yeahhhh");
    }


}