package com.example.kenny.pawpal;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kenny on 2/24/15.
 */
@ParseClassName("Pet")
public class Pet extends ParseObject {
    List<String> list;

    public Pet() {

    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        put("title", title);
    }

    public ParseUser getAuthor() {
        return getParseUser("author");
    }

    public void setAuthor(ParseUser user) {
        put("author", user);
    }

    public ParseFile getPhotoFile() {
        return getParseFile("photo");
    }

    public void setPhotoFile(ParseFile file) {
        put("photo", file);
    }

    public void setPetBirthday(String birthday) { put("petBirthday", birthday);}

    public void setPetVacday(String vacday) { put("petVacdate", vacday);}

    public String getPetBirthday() { return getString("petBirthday"); }

    public ArrayList<String> getPrevious() {

        List<String> list = getList("previousList");
        ArrayList arrayList = new ArrayList<String>(list);
        return arrayList;
    };

    public void setPrevious(List<String> List) {
        put("previousList", List);
    }

    public ArrayList<String> getUpcoming() {

        List<String> list = getList("upcomingList");
        ArrayList arrayList = new ArrayList<String>(list);
        return arrayList;
    };

    public void setUpcoming(List<String> List) {
        put("upcomingList", List);
    }



    public void setGender(int sex) {
        put("gender", sex);
        // 0 = male
        // 1 = female
    }

    public int getGender() {
        return getInt("gender");
    }





}

