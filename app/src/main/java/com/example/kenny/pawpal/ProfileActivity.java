package com.example.kenny.pawpal;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Settings;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.InputStream;
import java.net.URL;
import java.util.List;


public class ProfileActivity extends Activity {

    static final int REFRESH = 1;

    private GraphUser user;

    private TextView textViewResults;
    private ProfilePictureView profilePictureView;
    private String username;
    private String userId;
    private TextView petNameLabel;
    private TextView petGenderLabel;
    private TextView petBirthdayLabel;
    private String myPetName;
    private int myPetGender;
    private String myPetBDay;
    private ParseImageView petImg;
    private ImageButton addbtn;
    private ToggleButton tbtnLocation;
    private ToggleButton tbtnPetSitting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        tbtnLocation = (ToggleButton) this.findViewById(R.id.allow_location);
        tbtnPetSitting = (ToggleButton) this.findViewById(R.id.allow_petSitting);
       doBatchRequest();
        addbtn = (ImageButton) this.findViewById(R.id.btn_addPet);


        if(HasPet()) {
            Log.d("pet?","why??");
            //addbtn.setEnabled(false);
            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Toast.makeText(ProfileActivity.this, "You have already added a pet.", Toast.LENGTH_LONG).show();
                    addbtn.setEnabled(false);
                }
            });
        } else {
            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    addPet(v);
                }
            });
            Log.d("nopet?","why??");
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Parse.initialize(this, "aCOotHRXjfUnYHVLo4cNyuFNJHh1JldVk2kNWG4A",
                "bJPnV7xkkfrYjR95gwrFFtY5gvY7Wzuab6jXZk99");

        if(HasPet()) {
            Log.d("pet?","why??");
            //addbtn.setEnabled(false);
            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Toast.makeText(ProfileActivity.this, "You have already added a pet.", Toast.LENGTH_LONG).show();
                    addbtn.setEnabled(false);
                }
            });
        } else {
            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    addPet(v);
                }
            });
            Log.d("nopet?","why??");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void doBatchRequest() {
        textViewResults = (TextView) this.findViewById(R.id.textViewResults);
        textViewResults.setText("");
        profilePictureView = (ProfilePictureView) this.findViewById(R.id.profile_pic);
        petBirthdayLabel = (TextView) this.findViewById(R.id.petBirthday_label);
        petGenderLabel = (TextView) this.findViewById(R.id.petGender_label);
        petNameLabel = (TextView) this.findViewById(R.id.petName_label);
        petImg = (ParseImageView) this.findViewById(R.id.icon);

        String[] requestIds = {"me", "2"};

        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
        RequestBatch requestBatch = new RequestBatch();

        for (final String requestId : requestIds) {


            requestBatch.add(new Request(Session.getActiveSession(),
                    requestId, null, null, new Request.Callback() {
                public void onCompleted(Response response) {
                    GraphObject graphObject = response.getGraphObject();
                    String s = textViewResults.getText().toString();
                    if (graphObject != null) {
                        if (graphObject.getProperty("id") != null) {
                            username = (String) graphObject.getProperty("name");
                            userId = (String) graphObject.getProperty("id");


                            //CalendarActivity ca = new CalendarActivity();
                            //ca.setUser(userId);
                            tbtnLocation.setChecked(ParseUser.getCurrentUser().getBoolean("allowLocation"));
                            tbtnPetSitting.setChecked(ParseUser.getCurrentUser().getBoolean("allowPetSitting"));
                            String.format("Name : %s", username);

                            s = s + String.format("%s\n", graphObject.getProperty("name"));
                            s = s + String.format("%s\n" , graphObject.getProperty("birthday"));
                            s = s + String.format("%s\n", graphObject.getProperty("gender"));

                            Log.i("System.out", "the user id is"+userId);

                            profilePictureView.setProfileId(userId);
                        }

                    }

                    textViewResults.setText(s);

                }

            }));
        }
        requestBatch.executeAsync();


        // request pet data


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Pet");
        query.whereEqualTo("author", ParseUser.getCurrentUser());

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> listOfPets, ParseException e) {
                if (e == null && listOfPets.size()>0) {

                    myPetName = (String) listOfPets.get(0).getString("title");
                    myPetGender = (int) listOfPets.get(0).getInt("gender");
                    myPetBDay = (String) listOfPets.get(0).getString("petBirthday");

                    String petBirthday = "";
                    petBirthday = petBirthday + myPetBDay;
                    petBirthdayLabel.setText(petBirthday);

                    String petName = "";
                    petName = petName + myPetName;
                    petNameLabel.setText(petName);

                    if (myPetGender == 0) { // male
                        petGenderLabel.setText("Male");
                    } else {
                        petGenderLabel.setText("Female");
                    }

                    ParseImageView mealImage = petImg;
                    ParseFile photoFile = listOfPets.get(0).getParseFile("photo");
                    if (photoFile != null) {
                        mealImage.setParseFile(photoFile);
                        mealImage.loadInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                // nothing to do
                            }
                        });
                    }

                } else {

                }
            }
        });

    }

    public Bitmap getUserPic(String userID) {
        String imageURL;
        Bitmap bitmap = null;
        Log.i("System.out", "Loading Picture");
        imageURL = "http://graph.facebook.com/"+userID+"/picture?type=small";
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(imageURL).getContent());
        } catch (Exception e) {
            Log.d("TAG", "Loading Picture FAILED");
            e.printStackTrace();
        }
        return bitmap;
    }

    public void setUser(GraphUser user) {
        this.user = user;
    }

    public void petSittingToggle(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            ParseUser.getCurrentUser().put("allowPetSitting", on);
            //ParseUser.getCurrentUser().saveInBackground();
            ParseUser.getCurrentUser().saveEventually();
            Log.i("System.out","Pet-Sitting is on");
        } else {
            ParseUser.getCurrentUser().put("allowPetSitting", on);
            //ParseUser.getCurrentUser().saveInBackground();
            ParseUser.getCurrentUser().saveEventually();
            Log.i("System.out","Pet-Sitting is off");
        }
    }

    public void locationToggle(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();

        if (on) {
            ParseUser.getCurrentUser().put("allowLocation", on);
            //ParseUser.getCurrentUser().saveInBackground();
            ParseUser.getCurrentUser().saveEventually();
            Log.i("System.out","location sharing is on");
        } else {
            ParseUser.getCurrentUser().put("allowLocation", on);
            //ParseUser.getCurrentUser().saveInBackground();
            ParseUser.getCurrentUser().saveEventually();
            Log.i("System.out","location sharing is off");
        }
    }

    public void addPet(View view) {
        Intent intent = new Intent(view.getContext(), AddPetActivity.class); //fixed
        startActivity(intent);

    }

    private boolean HasPet() {
        if(ParseUser.getCurrentUser().getString("pet1_ID")!=null) {
            String id = ParseUser.getCurrentUser().getString("pet1_ID");
            Log.d("id?????????",id);
            return true;
        } else {
            return false;
        }
    }

}
