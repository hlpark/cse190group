package com.example.kenny.pawpal;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Arrays;
import java.util.List;

public class MainFragment extends MainFragmentBase {

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private Button returnButton;
    private Button facebookLoginButton;
    private LoginButton authButton;
    private TextView textViewResults;
    private GraphUser graphUser;
    private StartActivity sa = new StartActivity();
    private View view;
    private MainConfig config;
    private boolean off = false;

    List<String> permissions;
    private MainLoginSuccessListener onLoginSuccessListener;

    private static final String USER_OBJECT_NAME_FIELD = "username";


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.activity_main, container, false);
        Log.d("oncreateview","oncreateview");



        permissions = Arrays.asList("email", ParseFacebookUtils.Permissions.User.ABOUT_ME);


        facebookLoginButton = (Button) view.findViewById(R.id.facebook_login);

        setUpFacebookLogin();

        returnButton = (Button) view.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), sa.getClass()); //fixed
                startActivity(intent);
            }
        });
        authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                MainFragment.this.graphUser = user;

                if(isLoggedIn()) {
                    Intent intent = new Intent(view.getContext(), sa.getClass()); //fixed
                    startActivity(intent);
                }
            }


         });


        authButton.setFragment(this);


           return view;
        //delete later
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {

        super.onResume();

        // For scenarios where the main activity is launched and user
        // session is not null, the session state change notification
        // may not be triggered. Trigger it if it's open/closed.
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {

        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        if(isLoggedIn()) {
            returnButton.setVisibility(View.VISIBLE);
            authButton.setVisibility(View.VISIBLE);
            facebookLoginButton.setVisibility(View.INVISIBLE);
        } else {
            returnButton.setVisibility(View.INVISIBLE);
            authButton.setVisibility(View.INVISIBLE);
            facebookLoginButton.setVisibility(View.VISIBLE);
        }
    }

    private boolean isLoggedIn() {
        Session session = Session.getActiveSession();

        return (session != null && session.isOpened());

    }

    private void setUpFacebookLogin() {

        facebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingStart(true);
                ParseFacebookUtils.logIn(Arrays.asList(ParseFacebookUtils.Permissions.User.EMAIL, ParseFacebookUtils.Permissions.Friends.ABOUT_ME, ParseFacebookUtils.Permissions.User.PHOTOS,
                                ParseFacebookUtils.Permissions.User.HOMETOWN, ParseFacebookUtils.Permissions.User.LOCATION, ParseFacebookUtils.Permissions.User.BIRTHDAY),
                        getActivity(), new LogInCallback() {
                            @Override
                            public void done(ParseUser user, ParseException e) {
                                if (isActivityDestroyed()) {
                                    return;
                                }

                                if (user == null) {
                                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                                    loadingFinish();
                                    if (e != null) {
                                        showToast(R.string.com_parse_ui_facebook_login_failed_toast);
                                        debugLog(getString(R.string.com_parse_ui_login_warning_facebook_login_failed) +
                                                e.toString());
                                    }
                                } else if (user.isNew()) {
                                    Log.d("MyApp", "User signed up and logged in through Facebook!");
                                    Request.newMeRequest(ParseFacebookUtils.getSession(),
                                            new Request.GraphUserCallback() {
                                                @Override
                                                public void onCompleted(GraphUser fbUser,
                                                                        Response response) {
                      /*
                        If we were able to successfully retrieve the Facebook
                        user's name, let's set it on the fullName field.
                      */
                                                    ParseUser parseUser = ParseUser.getCurrentUser();
                                                    if (fbUser != null && parseUser != null
                                                            && fbUser.getName().length() > 0) {
                                                        parseUser.put(USER_OBJECT_NAME_FIELD, fbUser.getName());
                                                        if(fbUser.getBirthday() != null) {
                                                            parseUser.put("birthday", fbUser.getBirthday());
                                                        }
                                                        parseUser.put("allowLocation", off);
                                                        parseUser.put("allowPetSitting", off);
                                                        parseUser.put("facebookID", fbUser.getId());
                                                        /*parseUser.saveInBackground(new SaveCallback() {
                                                            @Override
                                                            public void done(ParseException e) {
                                                                if (e != null) {
                                                                    debugLog(getString(
                                                                            R.string.com_parse_ui_login_warning_facebook_login_user_update_failed) +
                                                                            e.toString());
                                                                }
                                                                loginSuccess();
                                                            }
                                                        });*/
                                                        parseUser.saveEventually();
                                                    }
                                                    loginSuccess();

                                                }
                                            }
                                    ).executeAsync();
                                } else {
                                    Log.d("MyApp", "User logged in through Facebook!");
                                    loginSuccess();
                                }
                            }
                        });
            }
        });
    }

    private void loginSuccess() {
        Log.d("LogIn", "Login Success");
     //   onLoginSuccessListener.onLoginSuccess();
    }






}
