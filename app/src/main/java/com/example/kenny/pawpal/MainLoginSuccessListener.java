package com.example.kenny.pawpal;

/**
 * Created by Kenny on 2/25/15.
 */
public interface MainLoginSuccessListener {

    public void onLoginSuccess();

}
